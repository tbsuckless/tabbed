# tabbed

This is my fork of the Suckless [tabbed](https://tools.suckless.org/tabbed) utility. This is designed to work with my forks of [st](https://github.com/Babkock/st) and [Surf](https://github.com/Babkock/surf), and the colors are the same as the ones in [Dotfiles](https://github.com/Babkock/Dotfiles).

## Patches

* [Alpha](https://tools.suckless.org/tabbed/patches/alpha)
* [Autohide](https://tools.suckless.org/tabbed/patches/autohide)

## Key bindings

* `Ctrl + Shift + Return` - new tab
* `Ctrl + Shift + H` - previous tab
* `Ctrl + Shift + L` - next tab
* `Ctrl + Shift + J` - move selected tab to the left
* `Ctrl + Shift + K` - move selected tab to the right
* `Ctrl + Tab` - jump to last selected tab
* `Ctrl + Q` - close tab

